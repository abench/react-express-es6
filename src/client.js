import React from 'react';
import ReactDOM from 'react-dom';
import Something from './someComponent';

ReactDOM.render(
	<Something />,
	document.getElementById('content')
);